FROM rekgrpth/python

RUN apk add --no-cache --virtual .build-deps \
        findutils \
        gcc \
        git \
        libc-dev \
        linux-headers \
        make \
    && mkdir -p /usr/src \
    && git clone --progress --recursive https://github.com/RekGRpth/unit.git /usr/src/unit \
    && cd /usr/src/unit \
    && ./configure --prefix=/usr/local \
    && ./configure python --config=python-config \
    && make -j$(nproc) all \
    && make install \
    && find /usr/local -type f -executable -not \( -name '*tkinter*' \) -exec scanelf --needed --nobanner --format '%n#p' '{}' ';' \
        | tr ',' '\n' \
        | sort -u \
        | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
        | xargs -rt apk add --no-cache --virtual .unit-rundeps \
    && apk del .build-deps \
    && cd / \
    && rm -rf /usr/src
